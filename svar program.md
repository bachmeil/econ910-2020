# SVAR program

Note: This is the R Markdown file for the *full* discussion of identification from our Zoom meetings Oct 29, Nov 3, and Nov 5. It demonstrates identification in general terms, with any restrictions imposed on the impact responses or the structural shock variances, and sign restrictions. You will want to save this with a .Rmd extension to use it in RStudio. I have to use a .md extension to get it to display properly on Gitlab.

There are two parts to the computation of IRFs. The first is the identification of impact responses to the shocks. For instance, in our example we have AS and AD shocks. Neither is something you observe in the data. If we want the IRF for inflation following an AD shock, the identification step is to identify the effect of an AD shock on the two variables. The second step is to compute the change in forecasts of inflation when the shock occurs. Forecasting with a VAR model is simple once we understand how to use recursion.

```{r}
F <- matrix(NA, nrow=2, ncol=2)
F[1,] <- c(0.5, 0.3)
F[2,] <- c(0.2, 0.8)
F # Confirm that it’s right
rec <- function(result, element) {
  return(F %*% result)
} 
```

Start the recursion.

- zero intercept
- zero pre-sample values
- forecasts for the shock vs no-shock cases

```{r}
Reduce(rec, 1:12, init=c(0.0, 0.0), accumulate=TRUE)
```

Base case: all forecasts are 0. That means, in a linear VAR model, we can compute the IRF as the forecast of inflation when all lagged values of both variables are zero and the intercept is zero.

Impact effect of the shock: y + 1, x + 0.5

```{r}
Reduce(rec, 1:12, init=c(1.0, 0.5), accumulate=TRUE)
```

```{r}
library(tstools)
data.raw <- read.csv("https://gitlab.com/bachmeil/econ910-2020/-/snippets/2016086/raw",
                     header=TRUE)
dataset <- ts(data.raw, start=c(1948,1), frequency=12)
u <- dataset[,"u"]
inf <- dataset[,"inf"]
```

Estimate RF VAR with vars package:

```{r}
library(vars)
fit <- VAR(dataset, p=6)
fit
```

You can conveniently get a particular row of F using the `Acoef` function from the `vars` package.

```{r}
a <- Acoef(fit)
unlist(lapply(a, function(mat) { mat["inf",] }))
```

# Sign restrictions

- We identify a *set* of impulse response functions by imposing restrictions on the "acceptable" signs of some of the responses.
- Object of interest: The response of inflation to an AD shock.
- Referring to the handout: We don't know values of b or c. They cannot be estimated directly from the data. We can't impose timing restrictions because we don't have any that we can justify.
- What we *can* say is that an AS shock should cause inflation to rise (normalization). An AS shock should never cause inflation to fall at any time horizon.
- An AS shock will cause u and inflation to move in the same direction at all time horizons.
- Sign restrictions help us to identify a set of "acceptable" values of b, c. They will identify the set of all b and c with "acceptable" impulse response functions, even though at most one of them is correct. We'll have a set of impulse response functions that includes the correct one but also a lot of wrong ones.
- Since we're interested in the response to an AD shock, we impose restrictions on the response to an AS shock. If we imposed restrictions on the response to AD shocks, it's not clear what we'd be identifying, since we'd be assuming our results rather than pulling them from the data.

# Moment restrictions

We will solve this using GMM.

- As shown in the handout: 3 equations, 4 unknowns
- If we impose a value for b, we can solve for c
- Once we have b and c, we can calculate the impact responses of both variables. That gives us `init` to start the recursion.
- Compute IRFs and check if they meet the conditions.
- Try many values of b.
- If IRFs meet conditions, then save b and c.
- If not, discard b and c.
- When I'm done, I'll have a set of b and c that meet the conditions on the IRFs.

```{r}
fit.u <- tsreg(u, lags(dataset,1))
fit.inf <- tsreg(inf, lags(dataset,1))
res1 <- fit.u$resids
res2 <- fit.inf$resids
s2.1 <- var(res1)
s2.2 <- var(res2)
s12 <- cov(res1,res2)
s2.1
s2.2
s12
```

`nleqslv`: Package and function to solve systems of nonlinear equations.

Return a vector of deviations from each of the equations.

```{r}
b <- 0 # recursiveness
objfnc <- function(par) {
  c <- par[1]
  advar <- par[2]
  asvar <- par[3]
  dev1 <- 0.04350375 - advar - b^2 * asvar
  dev2 <- 0.07218054 - c^2*advar - asvar
  dev3 <- -0.003451332 - c*advar - b*asvar
  return(c(dev1, dev2, dev3))
}
library(nleqslv)
soln <- nleqslv(c(0, 0.1, 0.1), objfnc)
soln$x[1] # Solution for c
```

Now using optim:

```{r}
objfnc2 <- function(par) {
  c <- par[1]
  advar <- par[2]
  asvar <- par[3]
  dev1 <- 0.04350375 - advar - b^2 * asvar
  dev2 <- 0.07218054 - c^2*advar - asvar
  dev3 <- -0.003451332 - c*advar - b*asvar
  return(dev1^2 + dev2^2 + dev3^2)
}
optim(c(0, 0.1, 0.1), objfnc2)
```

Now have a, b, c, d -> can convert structural shocks into reduced form residuals.

```{r}
F <- matrix(NA, ncol=2, nrow=2)
F[1,] <- coef(fit.u)[2:3]
F[2,] <- coef(fit.inf)[2:3]
F
```

Set up the recursion:

```{r}
rec <- function(result, element) {
  return(F %*% result)
}
```

Handle the contemporaneous part. What is the impact effect of the AS shock on u and inf?

b and 1

```{r}
irf.as <- Reduce(rec, 1:3, init=c(0,1),
                 accumulate=TRUE)
irf.as
```

Effect of AD shocks?

1 and c

```{r}
irf.ad <- Reduce(rec, 1:3, init=c(1,-0.079),
                 accumulate=TRUE)
irf.ad
```

Consistent with ECON 101: AD shock causes u and inf to move in opposite directions.

# Sign restrictions

- Want: Response of inflation to AD shock
- Impose restrictions on IRFs following AS shock: All IRFs are positive following AS shock

```{r}
u.as <- sapply(irf.as, function(h) {
  h[1]
})
u.as
inf.as <- sapply(irf.as, function(h) {
  h[2]
})
inf.as
```

Check the sign restrictions:

```{r}
restrictions.check <- (all(u.as >= 0) &
                         all(inf.as >= 0))
restrictions.check
```

$b=0$ is *acceptable*. Now we have to identify all elements of b. Set up a function that returns the IRFs for any choice of b.

```{r}
library(nleqslv)
irf.solve <- function(b) {
  objfnc <- function(par) {
    c <- par[1]
    advar <- par[2]
    asvar <- par[3]
    dev1 <- 0.04350375 - advar - b^2 * asvar
    dev2 <- 0.07218054 - c^2*advar - asvar
    dev3 <- -0.003451332 - c*advar - b*asvar
    return(c(dev1, dev2, dev3))
  }
  soln <- nleqslv(c(0, 0.1, 0.1), objfnc)
  c.solution <- soln$x[1]
  irf.as <- Reduce(rec, 1:3, init=c(b,1),
                   accumulate=TRUE)
  u.as <- sapply(irf.as, function(h) {
    h[1]
  })
  inf.as <- sapply(irf.as, function(h) {
    h[2]
  })
  irf.ad <- Reduce(rec, 1:3, 
                   init=c(1,c.solution), 
                   accumulate=TRUE)
  u.ad <- sapply(irf.ad, function(h) {
    h[1]
  })
  inf.ad <- sapply(irf.ad, function(h) {
    h[2]
  })
  return(list(u.as=u.as,
              inf.as=inf.as,
              u.ad = u.ad,
              inf.ad = inf.ad,
              b=b, c=c.solution))
}
irf.solve(0.2)
```

What's left: Call the function for many values of b. We'll need to make use of the `Filter` function.

```{r}
Filter(function(z) { z > 0 }, seq(-5,5))
```

```{r}
b.all <- seq(-0.5, 0.5, by=0.1)
irfs.all <- lapply(b.all, function(b.possible) {
  irf.solve(b.possible)
})
```

```{r}
acceptable <- Filter(function(this.irf) {
  all(this.irf$u.as >= 0) & 
    all(this.irf$inf.as >= 0)
}, irfs.all)
acceptable
```

```{r}
collect <- function(name) {
  sapply(acceptable, function(irf) {
    irf[[name]]
  })
}
inf.ad <- collect("inf.ad")
```

We now have the full set of acceptable IRFs. Get the largest and smallest IRF at each time horizon.

```{r}
max(inf.ad[1,])
max.inf.ad <- apply(inf.ad, 1, max)
min(inf.ad[1,])
min.inf.ad <- apply(inf.ad, 1, min)
```

```{r}
ceiling <- ts(max.inf.ad)
floor <- ts(min.inf.ad)
plot(ts.combine(ceiling, floor), plot.type="single",
     main="Identified Set of Inflation Responses to AD Shocks",
     xlab="", ylab="")
```

We're done with sign restrictions. Note that you could impose other assumptions.

```{r}
c <- 1.4 # recursiveness
objfnc <- function(par) {
  b <- par[1]
  advar <- par[2]
  asvar <- par[3]
  dev1 <- 0.04350375 - advar - b^2 * asvar
  dev2 <- 0.07218054 - c^2*advar - asvar
  dev3 <- -0.003451332 - c*advar - b*asvar
  return(c(dev1, dev2, dev3))
}
library(nleqslv)
soln <- nleqslv(c(0, 0.1, 0.1), objfnc)
soln$x[1] # Solution for c
```

If you impose that the structural shock variances are the same (would be hard to justify, but *if* you wanted, you could do it).

```{r}
objfnc <- function(par) {
  b <- par[1]
  c <- par[2]
  advar <- par[3]
  dev1 <- 0.04350375 - advar - b^2 * advar
  dev2 <- 0.07218054 - c^2*advar - advar
  dev3 <- -0.003451332 - c*advar - b*advar
  return(c(dev1, dev2, dev3))
}
library(nleqslv)
soln <- nleqslv(c(0, 0.1, 0.1), objfnc)
soln$x[1] # Solution for c
```
