This program demonstrates that you get the same AR coefficients in an AR model for the original variable or the demeaned variable. In the latter case, you would be predicting the deviation from the mean. After making a prediction, you'd have to add the mean back in to predict the variable itself. In fact, R's `arima` function actually reports results for the deviation from mean.

Working the deviation from the mean is usually more convenient. For many purposes, we only care about the dynamics, not the intercept.

**Warning:** In practice there will be very slight differences in the reported coefficients. That's because you lose one observation when taking the lag, but you calculate the mean using the entire sample.

```
library(tstools)
dgdp.raw <- read.csv("https://gitlab.com/-/snippets/2007800/raw", header=FALSE)
dgdp <- ts(dgdp.raw[,2], start=c(1947,2), frequency=4)
tsreg(dgdp, lags(dgdp, 1))
mu <- mean(dgdp)
mu
dgdp.mod <- dgdp - mu
tsreg(dgdp.mod, lags(dgdp.mod, 1))
```
