$b=c=0$

Reduced form residuals uncorrelated

Approaches:

- FIML
- GMM

```{r}
library(tstools)
library(gmm)
library(tstools)
data.raw <- read.csv("https://gitlab.com/bachmeil/econ910-2020/-/snippets/2016086/raw",
                     header=TRUE)
dataset <- ts(data.raw, start=c(1948,1), frequency=12)
u <- dataset[,"u"]
inf <- dataset[,"inf"]
fit.u <- tsreg(u, lags(dataset,1))
fit.inf <- tsreg(inf, lags(dataset,1))
```

Formulate the objective function

- nleqslv: Vector of moment conditions
- gmm: Matrix holding deviations on an observation
by observation basis.
- Three moment conditions
- T observations
- Return (Tx3) matrix
- Set sum of columns as close to zero as possible
- Row dimension (observations) used to choose the weighting matrix

```{r}
objfnc.oid <- function(par, residuals) {
  su <- par[1]
  sinf <- par[2]
  # On average, this is zero
  dev1 <- residuals[,"resu"]^2 - su
  dev2 <- residuals[,"resinf"]^2 - sinf
  dev3 <- residuals[,"resu"]*residuals[,"resinf"]
  return(cbind(dev1, dev2, dev3))
}
```

```{r}
res <- ts.combine(fit.u$resids, fit.inf$resids)
colnames(res) <- c("resu", "resinf")
objfnc.oid(c(0.1, 0.1), res)
```

```{r}
gmm(objfnc.oid, res, t0=c(0.1, 0.1))
```

# Identification by heteroskedasticity

- Mostly supply shocks -> All points on one demand curve, cor(p,q) < 0
- Mostly demand shocks -> All points on one supply curve, cor(p,q) > 0
- OLS estimates of the relationship change when jumping between regimes
- Coefficients stay the same
- Assumption of coefficient stability may or may not be easy to justify

# Nonlinear IRF analysis

- Regime switching models
    - Markov switching
    - y(t) = 0.2 + 0.7 y(t-1)
    - Threshold models
    - Model change in coefficients
    - GDP growth over the business cycle
    - y(t) = 0.002 + 0.9 y(t-1)
    - y(t) = 0.002 + 0.3 y(t-1)
- Machine learning: black box

```{r}
dgdp.raw <- read.csv(
  "https://gitlab.com/-/snippets/2007800/raw/master/dgdp.csv",
  header=TRUE)
dgdp <- ts(dgdp.raw[,2], start=c(1947,2), frequency=4)
```

Now set up and estimate dummy variable model.

Identify the positive growth periods with a dummy.

```{r}
d <- dgdp > 0
egrowth <- d*dgdp
fit.nl <- tsreg(dgdp, ts.combine(lags(dgdp,1), lags(egrowth,1)))
fit.nl
```

For recession:
$$y_{t} = 0.47 + 0.60 y_{t-1}$$

For expansion:
$$y_{t} = 0.47 + 0.38 y_{t-1}$$

Calculate the IRFs for the two regimes.

- Calculate for the two regimes separately.
- Might switch between the regimes.
- Expansion regime: negative shock -> switch regimes
- Recession regime: positive shock -> switch regimes

Depends on lots of factors.

Monte Carlo integration

- Draw everything random
- Make a forecast
- Do it many times
- Take the average

IRF: How the forecast of GDP changes with the shock vs without the shock.

Do this calculation for many initial values

and for many time paths of the shocks

Average of IRF

Depend on the assumptions you make



